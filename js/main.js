$(document).ready(function(){
	/* Animacion de secciones */
	wow = new WOW({
		animateClass: 'animated',
		offset: 100,
		callback: function (box) {
		}
	});
	wow.init();
	
	
	/* Arreglo para icono de menu */
	$('#nav-icon4').click(function(){
		$(this).toggleClass('open');
	});
	
	/* Arreglo para h1 con fondo de colores */
	var mouseX, mouseY;
	var ww = $( window ).width();
	var wh = $( window ).height();
	var traX, traY;
	$(document).mousemove(function(e){
		mouseX = e.pageX;
		mouseY = e.pageY;
		traX = ((4 * mouseX) / 570) + 40;
		traY = ((4 * mouseY) / 570) + 50;
		$('#moduloQue h1').css({'background-position': traX + '%' + traY + '%'});
	});
	
	/* Arreglo para flip en cajas de cliente y empreado */
	$('#moduloBases .col-md-4').flip({
		trigger: 'manual'
	});
	$('#cliente-btn').click(function(){
		$('#moduloBases .col-md-4').flip(true);
		$('#abcdijon-btn').show();
		$(this).hide();
	});

	$('#abcdijon-btn').click(function(){
		$('#moduloBases .col-md-4').flip(false);
		$('#cliente-btn').show();
		$(this).hide();
	});
	
	/* Arreglo para scroll en sitio */
	var windHeight = $(window).height();
	var altoMenu = $('nav.navbar.navbar-inverse').height();
	var altoMenuCont = windHeight - altoMenu;
	$( window ).scroll(function() {
		
		var scroll = $('body').scrollTop();
		if (scroll > altoMenuCont){
			$('.navbar.navbar-inverse').addClass('white');
		}else{
			$('.navbar.navbar-inverse').removeClass('white');
		}
	});
	$('.menu-superior').css('min-height', altoMenuCont + 'px' );
	$('#moduloQue').css('min-height', windHeight + 'px' );
	$('#moduloVideo').css('min-height', windHeight + 'px' );
	$('#moduloCliente').css('min-height', windHeight + 'px' );
	
	$( '#navbarToggleExternalContent a' ).on( "click", function() {
		$('#nav-icon4').trigger("click");
	});
	$('.gif-mouse').click(function(){
		scrollToModulo('.bottom-modulo');
	});
	$('.icon-vermas').click(function(){
		var idContenido = $(this).parent().attr("id");
		var idContenido = idContenido + 'Inner';
		document.getElementById(idContenido).style.width = "100%";
	});
	$('.closebtn').click(function(){
		var idContenido = $(this).parent().attr("id");
		document.getElementById(idContenido).style.width = "0%";
	});
	
	$('.slider-quienes').slick({
		centerMode: true,
		centerPadding: '360px',
		slidesToShow: 1,
		arrows: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
				}
			}
		]
	});
	
	$('.slider-para-quienes').slick({
		centerMode: false,
		slidesToShow: 1,
		arrows: false,
		focusOnSelect: true,
		infinite: true,
		dots: true,
		vertical: true
	});
	
	
});

function scrollToModulo(modulo){
	var modulo = $(modulo);
	var altoMenu = $('nav.navbar.navbar-inverse').height();
	var currentScrollTop = $('html,body').scrollTop();
	var nextScrollTop = modulo.offset().top;
	var speed = 100;
	var animation_time = Math.abs(nextScrollTop - currentScrollTop) / speed * 100;
	
	$('html,body').animate({
		scrollTop: nextScrollTop
	}, animation_time);
}